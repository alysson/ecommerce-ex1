1. Environment
	----
	1.1. Initialize docker containers
	1.2. Copy from .example files then set the configs in:
	- app/conf/db.php.example -> app/conf/db.php
	- app/db/auth/db.sqlite.example -> app/db/auth/db.sqlite
2. Populate database with create.php script:
	-
	- the script will create products and users (including login/password):
	-	cd /var/www/app
	`php create.php vendor 50 #create 50 users (type: vendor)`
	`php create.php product 100 #create 100 products (random vendors)`
	`php create.php customer 20 #create 20 customers`

3. Diagrams
    - 
    3.1. BPMN Diagram
    ![BPMN Diagram](bpmn-diagram.svg "BPMN Diagram")
    3.2. OOP Diagram
    ![OOP Diagram](oop-diagram.svg "OOP Diagram")
