FROM php:7.4-apache
RUN pecl info mongodb || (pecl install mongodb \
    &&  echo "extension=mongodb.so" > $PHP_INI_DIR/conf.d/mongo.ini)

RUN a2enmod rewrite
RUN a2enmod dbd
RUN a2enmod authn_dbd

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/
RUN install-php-extensions imagick
