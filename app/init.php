<?php

register_shutdown_function(function() { if(defined('IGNORE_ALL') && IGNORE_ALL) return; if($err = error_get_last()) { var_dump($err); }});

spl_autoload_register(function($className)
{
    $namespace=str_replace("\\","/",__NAMESPACE__);
    $className=str_replace("\\","/",$className);
    #$className = basename($className);
    $class=__DIR__ . "/src/".(empty($namespace)?"":$namespace."/")."{$className}.php";
    if(file_exists($class))
    include_once($class);
    #else{ var_dump($class); exit; }
});

require __DIR__ . '/vendor/autoload.php';
