const ProductViewModel = function(row){
    const self = this;

    if(row == undefined) row = {};

    self.sku = ko.observable(_.get(row, 'sku', ''));
    self.price = ko.observable(_.get(row, 'price', ''));
    self.shipment_delivery_times = ko.observable(_.get(row, 'shipment_delivery_times', ''));
    self.inventory = ko.observable(_.get(row, 'inventory', ''));
    self.active = ko.observable(_.get(row, 'active', true));
    self.images = ko.observableArray(_.get(row, 'images', []));
    self.images_preview = ko.observableArray(_.get(row, 'images_preview', []));
    self.categories = ko.observableArray(_.get(row, 'categories', []));
    self.vendor = ko.observableArray(_.get(row, 'vendor', ''));

    self.removeImage = function(rowimage){
        self.images.remove(rowimage);
    }

    self.save = function(){
        const data = JSON.parse(ko.toJSON(self));

        $.ajax({
            url: '/api/products/save',
            type: 'POST',
            data: data,
        }).then(function(resp){
            Swal.fire('Saved!', '', 'success').then(function(){
                setTimeout(function(){
                    window.location = '/products';
                }, 500);
            });
        }).fail(function(){
            Swal.fire('Error found.', '', 'error');
        });
    };

}

const vm = new ProductViewModel();
ko.applyBindings(vm);
