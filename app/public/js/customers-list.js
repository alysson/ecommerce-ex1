$(document).ready(function () {

    const render_col_has_quick_checkout = function ( data, type, row ) {
        const has_quick_checkout = _.toString(_.get(row, 'card.number', '')).length > 0;
        if (type === 'display') {
            if(has_quick_checkout)
                return '<i class="fa fa-check-square-o"></i>';
            return '<i class="fa fa-square-o"></i>';
        }
        return data;
    };

    $('#tb_customers').DataTable({
        ajax: '/api/customers.json',
        columns: [
            { title: 'email', data: 'email' },
            { title: 'Has 1-click checkout', data: null, render: render_col_has_quick_checkout, searchable: false },
        ],
    });
});
