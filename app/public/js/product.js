const handle_upload_image = function(inputfile){
    const uploadurl = '/api/products/upload';
    const oFile = inputfile.files[0];
    const fsize = oFile.size;
    const ftype = oFile.type;
    const max_file_size = 10;

    const image_filters = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;
    
    if(fsize > (max_file_size * 1024 * 1024)){
        alert("File size can not be more than " + max_file_size +"MB" );
        return false;
    }
    
    if(!image_filters.test(oFile.type)){
        alert("Unsupported file type!" );
        return false;
    }

    const fdata = new FormData();
    fdata.append('image_data', oFile);
        
    $.ajax({
        type: "POST",
        processData: false,
        contentType: false,
        url: uploadurl,
        data: fdata,
        dataType: 'json',
        beforeSend: $.blockUI,
        success:function(resp){
            const key = _.get(resp, 'key');
            const url = _.get(resp, 'url');
            if(key){
                vm.images.push(key);
            }

            if(url)
                vm.images_preview.push(url);
        }
    }).fail(function(e){
        Swal.fire('Error when uploading image.', '', 'error');
    }).always($.unblockUI);

    inputfile.value='';
};
