const StoreProductViewModel = function(row){
    let self = this;

    if(row == undefined) row = {};

    self.sku = ko.observable(_.get(row, 'sku', ''));
    self.price = ko.observable(_.toNumber(_.get(row, 'price', ''))
        .toLocaleString('en-US', {
            style: 'currency',
            currency: 'USD',
        }));
    self.vendor = ko.observable(_.get(row, 'vendor.name', ''));
    self.categories = ko.observableArray(_.get(row, 'categories', []));

    self.addedToCart = ko.observable(0);

    const image = _.first(_.get(row, 'images_preview'));

    self.image = ko.observable(image);

    self.visible = ko.pureComputed(function(){
        return vm.selectedCategories().length == 0 || _.intersection(vm.selectedCategories(), self.categories()).length;
    });

    self.addToCart = function(){
        const p = this;
        p.addedToCart(p.addedToCart()+1);
        vm.cartItems.push(p);
    };

};

const StoreViewModel = function(row){
    const self = this;

    if(row == undefined) row = {};

    self.products = ko.observableArray([]);
    self.cartItems = ko.observableArray([]);
    self.categories = ko.observableArray([]);
    self.selectedCategories = ko.observableArray([]);

    self.addToCart = function(){
        console.log(this);
    };

    self.fetchProducts = function(){
        $.ajax({
            url: '/api/store/products.json',
            beforeSend: $.blockUI,
        }).then(function(resp){
            const rows = _.get(resp, 'data');
            if(Array.isArray(rows)){
                for(let i = 0; i < rows.length; i++){
                    self.products.push(new StoreProductViewModel(rows[i]));
                }
            }
        }).always($.unblockUI);
    };

    self.fetchCategories = function(){
        $.ajax({
            url: '/api/store/categories.json',
            beforeSend: $.blockUI,
        }).then(function(resp){
            const rows = _.get(resp, 'data');
            if(Array.isArray(rows)){
                for(let i = 0; i < rows.length; i++){
                    self.categories.push(rows[i]);
                }
            }
        }).always($.unblockUI);
    };

    self.fetchProducts();

    self.fetchCategories();

    self.checkout = function(){
        $.ajax({
            url: '/api/store/checkout',
            beforeSend: $.blockUI,
            type: 'POST',
        }).then(function(resp){
            Swal.fire('Checkout completed!', 'Thank you for shopping with us!', 'success');
        }).fail(function(jqXHR, textStatus, errorThrown){
            let message = _.get(jqXHR, 'responseJSON.error', 'Error');
            Swal.fire(message, '', 'warning');
        }).always($.unblockUI);
    }
}

const vm = new StoreViewModel();
ko.applyBindings(vm);
