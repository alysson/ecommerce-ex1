const VendorViewModel = function(row){
    const self = this;

    if(row == undefined) row = {};

    self.name = ko.observable(_.get(row, 'name', ''));
    self.ein = ko.observable(_.get(row, 'ein', ''));
    self.vat = ko.observable(_.get(row, 'vat', ''));
    self.email = ko.observable(_.get(row, 'email', ''));
    self.password = ko.observable('');
    self.password_confirm = ko.observable('');

    self.save = function(){

        const data = JSON.parse(ko.toJSON(self));

        const p1 = _.get(data, 'password');
        const p2 = _.get(data, 'password_confirm');

        if(p1 && p1 != p2){
            Swal.fire('Invalid password confirm', '', 'warning');
            return false;
        }

        _.unset(data, 'password_confirm');

        $.ajax({
            url: '/api/vendors/save',
            type: 'POST',
            data: data,
        }).then(function(resp){
            Swal.fire('Saved!', '', 'success').then(function(){
                setTimeout(function(){
                    window.location = '/vendors';
                }, 500);
            });
        }).fail(function(){
            Swal.fire('Error found.', '', 'error');
        });
    };

}

const vm = new VendorViewModel();
ko.applyBindings(vm);
