$(document).ready(function () {

    const render_col_boolean = function ( data, type, row ) {
        if (type === 'display') {
            if(data == true)
                return '<i class="fa fa-check-square-o"></i>';
            return '<i class="fa fa-square-o"></i>';
        }
        return data;
    };

    const render_col_images = function ( data, type, row ) {
        if (type === 'display') {
            if(Array.isArray(data)){
                if(data.length){
                    const sku = _.get(row, 'sku', '');
                    const images = [];
                    for(let i = 0; i < data.length; i++){
                        const url = data[i];
                        images.push(`<img src="${url}" height="32px" class="me-1" alt="${sku} image">`);
                    }
                    return images.join('');
                }
            }
            return '';
        }
        return data;
    };

    $('#tb_products').DataTable({
        ajax: '/api/products.json',
        columns: [
            { title: 'Image', data: 'images_preview', render: render_col_images },
            { title: 'Vendor', data: 'vendor.name' },
            { title: 'SKU', data: 'sku' },
            { title: 'Price', data: 'price', type: 'num-fmt' },
            { title: 'Inventory', data: 'inventory', type: 'num' },
            { title: 'Shipment time (days)', data: 'shipment_delivery_times', type: 'num' },
            //{ data: 'images' },
            { title: 'Categories', data: 'categories',render: {
                    _: '[, ]',
                    sb:'[]'
                }},
            { title: 'Active', data: 'active', render: render_col_boolean, searchable: false },
        ],
    });
});
