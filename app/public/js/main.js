const logout = function(){
    let url = window.location.href.replace(/\/\//, '\/\/.@');
    $.ajax({
        url: url,
        beforeSend: $.blockUI
    }).always(function(){
            document.body.innerHTML='<p class="text-center m-t-lg display-5">Logged out.</p>';
    });
}
