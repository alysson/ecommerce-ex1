$(document).ready(function () {

    const render_col_boolean = function ( data, type, row ) {
        if (type === 'display') {
            if(data == true)
                return '<i class="fa fa-check-square-o"></i>';
            return '<i class="fa fa-square-o"></i>';
        }
        return data;
    };

    const render_col_images = function ( data, type, row ) {
        if (type === 'display') {
            if(Array.isArray(data)){
                if(data.length){
                    const url = _.get(data, '0.url', '');
                    const sku = _.get(row, 'sku', '');
                    if(url) return `<img src="${url}" alt="${sku} image">`;
                }
            }
            return '';
        }
        return data;
    };

    $('#tb_vendors').DataTable({
        ajax: '/api/vendors.json',
        columns: [
            { title: 'Name', data: 'name' },
            { title: 'EIN', data: 'ein' },
            { title: 'VAT', data: 'vat' },
            { title: 'Email', data: 'email' },
        ],
    });
});
