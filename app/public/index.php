<?php

require __DIR__ . '/../init.php';

// Create and configure Slim app
$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true,
]];

$app = new \Slim\App($config);

$container = $app->getContainer();

$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(__DIR__ . '/../templates', [
        //'cache' => __DIR__ . '/../tmp/cache'
    ]);

    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));

    return $view;
};

$container['view']['logged_user_email'] = \Model\AuthUser::getLoggedUserEmail();;
$container['view']['logged_user_role'] = \Model\AuthUser::getLoggedUserRole();
$container['view']['menu_items'] = \Model\AuthUser::getMenuItems();

\Routes::init($app);

$app->run();
