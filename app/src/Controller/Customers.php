<?php

namespace Controller;

class Customers extends Base {

    public function list_json($request, $response, $args){
        $model = new \Model\Customer;
        $rows = $model->findAll();
        return $response->withJson(['data' => $rows]);
    }

    public function index($request, $response, $args){
        $this->render($response, 'list.twig', $args);
    }

}
