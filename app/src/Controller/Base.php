<?php

namespace Controller;

abstract class Base {

    public $app;

    public function __construct($container){
        $this->app = $container;
    }

    public function index($request, $response, $args){
        $this->render($response, 'list.twig', $args);
    }

    public function render($response, $template, $args = []){
        $className = (new \ReflectionClass($this))->getShortName();
        $args['section'] = strtolower($className);
        return $this->app->view->render($response, $template, $args);
    }

}
