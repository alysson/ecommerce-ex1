<?php

namespace Controller;

class Products extends Base {

    const IMAGE_HEIGHT = 640;
    const IMAGE_WIDTH = 640;
    const IMAGE_FIELD = 'image_data';

    private function get_image_mime(){
        if(!isset($_FILES[self::IMAGE_FIELD]) || !is_uploaded_file($_FILES[self::IMAGE_FIELD]['tmp_name'])){
            return FALSE;
        }

        $image_info  = getimagesize($_FILES[self::IMAGE_FIELD]['tmp_name']);
        return $image_info && !empty($image_info['mime']) ? $image_info['mime'] : FALSE;
    }

   /*
    * https://www.php.net/manual/pt_BR/book.imagick.php
    * */
    private function resize_uploaded_image($image_path){
        try{
            $image = new \Imagick($image_path);
            $image->resizeImage(
                self::IMAGE_HEIGHT,
                self::IMAGE_WIDTH,
                \Imagick::FILTER_LANCZOS, //https://www.imagemagick.org/Usage/resize/#distort_resize
                1,
                TRUE
            );

            return $image->getImageBlob();
        }catch(\Exception $e){
        }
    }

    public function upload($request, $response, $args){
        $image_mime = $this->get_image_mime();
        if(!$image_mime)
            return $response->withStatus(400);

        $source_filename = $_FILES[self::IMAGE_FIELD]['name'];
        $ext = pathinfo($source_filename, PATHINFO_EXTENSION);

        $file_key = sprintf('products/%s.%s', \Util\General::uuid(), $ext);
        $image_path = $_FILES[self::IMAGE_FIELD]['tmp_name'];

        $resized = $this->resize_uploaded_image($image_path);
        if(!$resized) return $response->withStatus(500);

        $uploaded = \Util\Storage::putObject($resized, $file_key, $image_mime);
        if($uploaded !== TRUE)
            return $response->withStatus(500)->withJson(['error' => 'Error when uploading to storage.']);
        $signed_url = \Util\Storage::getSignedUrl($file_key);

        return $response->withJson(['key' => $file_key, 'url' => $signed_url]);
    }

    public function list_json($request, $response, $args){
        $model = new \Model\Product;
        $rows = $model->findAll();
        return $response->withJson(['data' => $rows]);
    }

    public function add($request, $response, $args){
        $args['arr_categories'] = \Util\Metadata::categories();
        $Vendor = new \Model\Vendor;
        $args['arr_vendors'] = $Vendor->findAll();
        $this->render($response, 'products/form.twig', $args);
    }

    public function save($request, $response, $args){
        $data = $request->getParams();
        $model = new \Model\Product;
        return $model->save($data);
    }

}
