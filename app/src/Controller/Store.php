<?php

namespace Controller;

class Store extends Base {

    public function list_json($request, $response, $args){
        $model = new \Model\Store;
        $rows = $model->getProducts();
        return $response->withJson(['data' => $rows]);
    }

    public function index($request, $response, $args){
        $this->render($response, 'store/home.twig', $args);
    }

    public function checkout($request, $response, $args){

        sleep(2);

        $Customer = new \Model\Customer;
        $customer = $Customer->getCurrent();
        if(empty($customer->card) || empty($customer->card->number)){
            return $response->withStatus(400)->withJson(['error' => 'One click checkout requires a credit card previously saved in your account.']);
        }

        return $response->withStatus(201);
    }

    public function list_json_categories($request, $response, $args){
        $model = new \Model\Store;
        $rows = $model->getCategories();
        return $response->withJson(['data' => $rows]);
    }

}
