<?php

namespace Controller;

class Vendors extends Base {

    public function add($request, $response, $args){
        $this->render($response, 'vendors/form.twig', $args);
    }

    public function list_json($request, $response, $args){
        $model = new \Model\Vendor;
        $rows = $model->findAll();
        return $response->withJson(['data' => $rows]);
    }

    public function save($request, $response, $args){
        $data = $request->getParams();
        $model = new \Model\Vendor;
        return $model->save($data);
    }

}
