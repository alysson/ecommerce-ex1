<?php

class Routes {

    public static function init($app){

        $app->get('/', function ($request, $response, $args) {
            $this->view->render($response, 'index.twig');
        });

        $app->get('/store', '\Controller\Store:index');
        $app->get('/api/store/products.json', '\Controller\Store:list_json');
        $app->get('/api/store/categories.json', '\Controller\Store:list_json_categories');
        $app->post('/api/store/checkout', '\Controller\Store:checkout');

        $app->get('/products', '\Controller\Products:index');
        $app->get('/products/add', '\Controller\Products:add');

        $app->get('/api/products.json', '\Controller\Products:list_json');
        $app->post('/api/products/save', '\Controller\Products:save');
        $app->post('/api/products/upload', '\Controller\Products:upload');

        $app->get('/customers', '\Controller\Customers:index');
        $app->get('/api/customers.json', '\Controller\Customers:list_json');

        $app->get('/vendors', '\Controller\Vendors:index');
        $app->get('/vendors/add', '\Controller\Vendors:add');
        $app->post('/api/vendors/save', '\Controller\Vendors:save');
        $app->get('/api/vendors.json', '\Controller\Vendors:list_json');

    }

}
