<?php

namespace Model;

class Customer extends User {

    public function find($id){
        return $this->db->dm->find(\Documents\Customer::class, $id);
    }

    public function findAll(){
        $repository = $this->db->dm->getRepository(\Documents\Customer::class);
        return $repository->findAll();
    }

    public static function getMenuItems(){
        return [
            ['action' => '/store', 'label' => 'Products'],
        ];
    }

}
