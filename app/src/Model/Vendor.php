<?php

namespace Model;

class Vendor extends User {

    public function find($id){
        return $this->db->dm->find(\Documents\Vendor::class, $id);
    }

    public function findAll(){
        $repository = $this->db->dm->getRepository(\Documents\Vendor::class);
        return $repository->findAll();
    }

    public function save($data){
        $vendor = new \Documents\Vendor;
        $vendor->name = \Util\General::get_value($data, 'name');
        $vendor->ein = \Util\General::get_value($data, 'ein');
        $vendor->vat = \Util\General::get_value($data, 'vat');
        $vendor->email = \Util\General::get_value($data, 'email');
        $pass = \Util\General::get_value($data, 'password');
        if($pass)
            $vendor->setPassword($pass);

        return $this->saveRow($vendor);
    }

    public static function getMenuItems(){
        return [
            ['action' => '/products', 'label' => 'Products'],
        ];
    }

}
