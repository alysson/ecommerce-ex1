<?php

namespace Model;

class Store extends Base {

    public function getProducts(){
        $repository = $this->db->dm->getRepository(\Documents\Product::class);
        $qb = $repository->createQueryBuilder();
        $qb->select('_id', 'sku', 'price', 'vendor.name', 'images', 'images_preview', 'categories');
        $qb->field('active')->equals(TRUE);
        $qb->limit(20);
        $qb->refresh();
        return $qb->getQuery()->toArray();
    }

    public function getCategories(){
        return \Util\Metadata::categories();
    }

}
