<?php

namespace Model;

class Product extends Base {

    public function save($data){
        $product = new \Documents\Product;
        $product->sku = \Util\General::get_value($data, 'sku');
        $product->price = (float) \Util\General::get_value($data, 'price');
        $product->inventory = (int) \Util\General::get_value($data, 'inventory');
        $product->active = \Util\General::get_value($data, 'active') == TRUE;
        $product->shipment_delivery_times = (int) \Util\General::get_value($data, 'shipment_delivery_times');
        $product->categories = \Util\General::get_value($data, 'categories');

        $images = \Util\General::get_value($data, 'images', []);

        if(is_array($images))
        foreach($images as $key_file){
            $row_image = new \Documents\ProductImage;
            $row_image->key = $key_file;
            $product->images[] = $row_image;
        }

        $product->images_preview = \Util\General::get_value($data, 'images_preview');

        return $this->saveRow($product);
    }

    public function find($id){
        return $this->db->dm->find(\Documents\Product::class, $id);
    }

    public function findAll(){
        $repository = $this->db->dm->getRepository(\Documents\Product::class);
        if(\Model\AuthUser::getLoggedUserRole() == 'vendor'){
            $Vendor = new \Model\Vendor;
            $vendor = $Vendor->getCurrent();
            return $repository->findBy(['vendor' => $vendor]);
        }
        return $repository->findAll();
    }

}
