<?php

namespace Model;

class Base {

    public $db;

    public function __construct(){
        $this->db = \Util\Db::getInstance();
    }

    public function saveRow($row){
        $this->db->dm->persist($row);
        $this->db->dm->flush();
    }

}
