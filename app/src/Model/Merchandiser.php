<?php

namespace Model;

class Merchandiser extends Base {

    public static function getMenuItems(){
        return [
            ['action' => '/products', 'label' => 'Products'],
            ['action' => '/products/add', 'label' => 'Add Product'],
            ['action' => '/vendors', 'label' => 'Vendors'],
            ['action' => '/vendors/add', 'label' => 'Add Vendor'],
        ];
    }

}
