<?php

namespace Model;

abstract class User extends Base {

    public static function getMenuItems(){
    }

    public function getCurrent(){
        $email = \Model\AuthUser::getLoggedUserEmail();
        $className = (new \ReflectionClass($this))->getShortName();
        $className = "\\Documents\\$className";
        $repository = $this->db->dm->getRepository($className);
        return $repository->findOneBy(['email' => $email]);
    }

}
