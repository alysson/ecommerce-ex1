<?php

namespace Model;

use PDO;

class AuthUser {

    private static function getAuthDb(){
        $sqlitedb = __DIR__ . '/../../db/auth/db.sqlite';
        $db = new PDO("sqlite:{$sqlitedb}", "", "", [
            PDO::ATTR_PERSISTENT => TRUE,
            PDO::ERRMODE_EXCEPTION => TRUE
        ]);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
    }

    private static function getbyemail($email){
        $db = self::getAuthDb();
        return $db->query("SELECT * FROM users where email = '{$email}'")->fetch(PDO::FETCH_ASSOC);
    }

    public static function getLoggedUserEmail(){
        return $_SERVER['PHP_AUTH_USER'];
    }

    public static function getLoggedUserRole(){
        $email = $_SERVER['PHP_AUTH_USER'];
        $user = self::getbyemail($email);
        return \Util\General::get_value($user, 'role');
    }

    public static function save($id, $email, $password, $role){
        $db = self::getAuthDb();

        try{
            $db->exec("DELETE FROM users WHERE ID = {$id}");
        }catch(\Exception $e){
        }

        $stmt = $db->prepare("INSERT INTO users (id, email, password, role) VALUES(?, ?, ?, ?)");
        $created = $stmt->execute([
            $id,
            $email,
            \Util\General::hash_password_auth($password),
            $role,
        ]);
        return $created;
    }

    public static function getMenuItems(){
        $role = self::getLoggedUserRole();
        $className = ucfirst($role);
        $model = "\Model\\$className";
        return $model::getMenuItems();
    }

}
