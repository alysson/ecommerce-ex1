<?php

namespace Documents;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/** @ODM\EmbeddedDocument */
class Address {

    /** @Field(type="string") */
    public string $street;

    /** @Field(type="string") */
    public string $city;

}
