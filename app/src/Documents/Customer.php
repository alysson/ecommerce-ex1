<?php

namespace Documents;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document as Document;
use Doctrine\Common\Collections\ArrayCollection;


/** @Document(collection="customers") */
class Customer extends User{

    /** @ODM\EmbedOne(targetDocument=Address::class, name="billing_address") */
    public $billingAddress = null;

    /** @ODM\EmbedOne(targetDocument=Address::class, name="shipping_address;") */
    public $shippingAddress = null;

    /** @ODM\EmbedOne(targetDocument=Card::class, name="card;") */
    public $card;

}
