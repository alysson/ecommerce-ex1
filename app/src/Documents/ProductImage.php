<?php

namespace Documents;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;

/** @ODM\EmbeddedDocument */

class ProductImage{
    /** @Field(type="string") */
    public string $key;

    /** @Field(type="date") */
    public \MongoDB\BSON\UTCDateTime $created_at;

    public string $url = '';

    public function __construct($key=''){
        if($key) $this->key = $key;
        $this->created_at = new \MongoDB\BSON\UTCDateTime();
    }
}
