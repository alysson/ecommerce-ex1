<?php

namespace Documents;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/** @ODM\EmbeddedDocument */
class Card {

    /** @Field(type="string") */
    public string $number;

    /** @Field(type="string") */
    public string $cvc;

    /** @Field(type="string") */
    public string $fullname;

    /** @Field(type="date") */
    public \DateTime $expiration;

}
