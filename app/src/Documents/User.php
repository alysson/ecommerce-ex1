<?php

namespace Documents;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\MappedSuperclass @ODM\HasLifecycleCallbacks */

abstract class User{
    /** @ODM\Id */
    public $id;

    /** @ODM\Field(type="string") */
    public string $email;

    /** @ODM\Field(type="string") */
    protected string $password;

    public function setPassword($p){
        $this->password = $p;
    }

    /** @ODM\PrePersist */
    public function prePersist(): void{
        if(!empty($this->password)){

            $className = strtolower((new \ReflectionClass($this))->getShortName());
            \Model\AuthUser::save($this->id, $this->email, sha1($this->password, TRUE), $className);

            $this->password = sha1($this->password);
        }
    }


}
