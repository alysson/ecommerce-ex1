<?php

namespace Documents;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document as Document;
use Doctrine\Common\Collections\ArrayCollection;


/** @Document(collection="vendors") */
class Vendor extends User{

    /** @ODM\Field(type="string") */
    public string $name;

    /** @ODM\Field(type="string") */
    public string $ein;

    /** @ODM\Field(type="string") */
    public string $vat;

}
