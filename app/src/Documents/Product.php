<?php

namespace Documents;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document as Document;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\PersistentCollection;
use Doctrine\ODM\MongoDB\Event\PreLoadEventArgs;

/** @Document(collection="products") @ODM\HasLifecycleCallbacks */
class Product {

    /** @ODM\Id */
    public $id;

    /** @ODM\Field(type="string") */
    public string $sku;

    /** @ODM\Field(type="float") */
    public float $price;

    /** @ODM\Field(type="int") */
    public int $inventory;

    /** @ODM\Field(type="bool") */
    public bool $active;

    /** @ODM\Field(type="int") */
    public int $shipment_delivery_times;

    /** @ODM\Field(type="collection") */
    public $images = [];

    /** @ODM\Field(type="collection") */
    public $images_preview = [];

    /** @ODM\Field(type="collection") */
    public $categories = [];

    /**
     * @ODM\EmbedOne(targetDocument=Vendor::class)
     */
    public $vendor;

}
