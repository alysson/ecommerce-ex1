<?php

namespace Util;

class Storage {

    const BUCKET = 'shop1';
    const DEFAULT_SIGNED_URL_TIME_EXPIRATION = 10;

    private static function s3Client(){
        return new \Aws\S3\S3Client([
            'credentials' => FALSE,
            'version' => 'latest',
            'region'  => 'us-east-1',
            'endpoint' => 'http://localstack:4566',
            'force_path_style' => TRUE,
            'use_path_style_endpoint' => true,
            #'debug' => true,
        ]);
    }

    public static function putObject($blob, $file_key, $contentType=''){
        $s3 = self::s3Client();
        try{

            $result = $s3->putObject(array(
                'Bucket' => self::BUCKET,
                'Key'    => $file_key,
                'Body'   => $blob,
                #'ACL'    => 'public-read',
                'ContentType' => $contentType
            ));

            return TRUE;
        }catch(\Exception $e){
        }
    }

    public static function getSignedUrl($file_key){
        $s3 = self::s3Client();
        return $s3->getObjectUrl(self::BUCKET, $file_key, sprintf('+%d minutes', self::DEFAULT_SIGNED_URL_TIME_EXPIRATION));
    }

}
