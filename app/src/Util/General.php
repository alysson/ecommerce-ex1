<?php

namespace Util;

class General {

    public static function uuid() {
        $data = PHP_MAJOR_VERSION < 7 ? openssl_random_pseudo_bytes(16) : random_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);    // Set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);    // Set bits 6-7 to 10
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    public static function get_value($var, $index, $default_value = NULL){
        if(!$var) return;
        if(method_exists($var, $index))
            return $var->{$index}();
        if(is_array($var) && isset($var[$index]))
            return $var[$index];
        if(is_object($var) && isset($var->{$index}))
            return $var->{$index};
        return $default_value;
    }

    public static function hash_password_auth($p){

        if(!preg_match('/^\{SHA\}/', $p)){
            $p = '{SHA}' . base64_encode($p);
        }

        return $p;
    }

}
