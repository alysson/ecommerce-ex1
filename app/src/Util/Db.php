<?php

namespace Util;

use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\ODM\MongoDB\Configuration;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;

use MongoDB\Client;

class Db {

    private static $_instance;
    private $client;
    private string $database_name;
    public $dm;

    private function __construct(){
        $this->dm = $this->initialize();
    }

    private function initialize(){
        $config_file = __DIR__ . '/../../conf/db.php';
        if(!file_exists($config_file)) throw new \Exception('Database config file missing.');
        $defaults = [
            'host' => 'localhost',
            'port' => '27017'
        ];
        $config = include $config_file;
        $config = array_merge($defaults, $config);

        $this->database_name = $config['database'];

        #return new \MongoDB\Driver\Manager("mongodb://{$config['host']}:{$config['port']}");

        $client = new Client("mongodb://{$config['host']}:{$config['port']}", [], ['typeMap' => DocumentManager::CLIENT_TYPEMAP]);
        $config = new Configuration();

        $DIR = __DIR__ . '/../../db';

        $config->setProxyDir($DIR . '/Proxies');
        $config->setProxyNamespace('Proxies');
        $config->setHydratorDir($DIR . '/Hydrators');
        $config->setHydratorNamespace('Hydrators');
        $config->setDefaultDB($this->database_name);
        $config->setAutoGenerateProxyClasses(Configuration::AUTOGENERATE_EVAL);


        $config->setMetadataDriverImpl(AnnotationDriver::create());
        //$config->setMetadataDriverImpl(AnnotationDriver::create(__DIR__ . '/../Documents'));


        return DocumentManager::create($client, $config);

    }

    private function namespace($c){
        return implode('.', [$this->database_name, $c]);
    }

    public static function getInstance(){
        if( true === is_null( self::$_instance ) ){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function insert($collection_name, $data){
        $bulk = new \MongoDB\Driver\BulkWrite;
        $bulk->insert($data);
        return $this->client->executeBulkWrite($this->namespace($collection_name), $bulk);
    }

}
