<?php

require __DIR__ . '/init.php';

class MyGenerateData{

private static function faker(){
    $faker = Faker\Factory::create();
    $faker->addProvider(new Faker\Provider\en_US\Person($faker));
    $faker->addProvider(new Faker\Provider\en_US\Address($faker));
    $faker->addProvider(new Faker\Provider\en_US\Company($faker));
    $faker->addProvider(new Faker\Provider\en_GB\Company($faker));
    return $faker;
}

public static function create_customer(){
    $faker = self::faker();
    $customer = new \Documents\Customer;
    $customer->email = $faker->email();
    $pass = rand(111111,999999);
    $customer->setPassword($pass);

    echo sprintf('Created CUSTOMER:%slogin: %s%spassword: %s'.PHP_EOL.PHP_EOL, PHP_EOL, $customer->email, PHP_EOL, $pass);

    $customer->billingAddress = new \Documents\Address;
    $customer->billingAddress->street = $faker->address();

    $customer->shippingAddress = new \Documents\Address;
    $customer->shippingAddress->street = $faker->address();

    $customer->card = new \Documents\Card;
    $customer->card->cvc = $faker->numberBetween(100, 999);
    $customer->card->expiration = $faker->creditCardExpirationDate(TRUE, 'Y-m');

    $random_card = $faker->creditCardDetails();
    $customer->card->fullname = $random_card['name'];
    $customer->card->number = $random_card['number'];


    $db = \Util\Db::getInstance();
    $c = $db->dm->persist($customer);
    $db->dm->flush();
}

public static function create_vendor(){
    $faker = self::faker();
    $vendor = new \Documents\Vendor;
    $vendor->name = $faker->company();
    $vendor->ein = $faker->ein();
    $vendor->vat = $faker->vat();

    $vendor->email = $faker->email();
    $pass = rand(111111,999999);
    $vendor->setPassword($pass);

    echo sprintf('Created VENDOR:%slogin: %s%spassword: %s'.PHP_EOL.PHP_EOL, PHP_EOL, $vendor->email, PHP_EOL, $pass);

    $db = \Util\Db::getInstance();
    $c = $db->dm->persist($vendor);
    $db->dm->flush();
}

public static function create_merchandiser(){
    $faker = self::faker();
    $row = new \Documents\Merchandiser;
    $row->email = $faker->email();
    $pass = rand(111111,999999);
    $row->setPassword($pass);

    echo sprintf('Created MERCHANDISER:%slogin: %s%spassword: %s'.PHP_EOL.PHP_EOL, PHP_EOL, $row->email, PHP_EOL, $pass);

    $db = \Util\Db::getInstance();
    $c = $db->dm->persist($row);
    $db->dm->flush();
}

public static function create_product(){
    $faker = self::faker();
    $product = new \Documents\Product;
    $product->sku = $faker->regexify('^[A-Z]{2}\d{4}[A-Z]{2}$');
    $product->price = $faker->randomFloat(2, 20, 999);
    $product->inventory = $faker->numberBetween(1, 99);
    $product->active = $faker->randomElement([TRUE, FALSE]);
    $product->shipment_delivery_times = $faker->numberBetween(1,7);

    $img = new \Documents\ProductImage;
    $img->key = $faker->imageUrl();

    $imageurl = $faker->imageUrl();

    $product->images[] = ['key' => $imageurl, 'url' => $imageurl];
    $product->images_preview[] = $imageurl;

    $product->categories[] = $faker->randomElement(\Util\Metadata::categories());

    $Vendor = new \Model\Vendor;
    #$product->vendor = $Vendor->find('62953d9cc85570af82049863');

    $product->vendor = $faker->randomElement($Vendor->findAll());


    $db = \Util\Db::getInstance();
    $c = $db->dm->persist($product);
    $db->dm->flush();
}

}

$type = $argv[1];
$len = empty($argv[2]) ? 1 : $argv[2];

for($i=0; $i< $len; $i++){
    $fn = "create_{$type}";
    MyGenerateData::$fn();
}
